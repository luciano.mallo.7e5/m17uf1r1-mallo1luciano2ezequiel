using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameStates
{
    Normal,
    Growing,
    Giant,
    GoingDown
};

public enum PlayerState
{
    Idle,
    Walk,
    Lose
}




public class PlayerData : MonoBehaviour
{
    [SerializeField]
    public PlayerState PlayerState = PlayerState.Idle;
    public string PlayerName; 
    public Animator animator;
    private float _timeInmune = 5f;
    private float _timeCurrentInmune;
    [SerializeField] private bool _playerIsInmune;

    private void Awake()
    {

        PlayerName = GameManager.GetPlayerName();
        GameManager.SetGameState(GameStates.Normal);
        _timeCurrentInmune = _timeInmune;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    //Update is called once per frame To move the objeto to the left
    void Update()
    {
        _playerIsInmune = GameManager.GetPlayerIsInmune();
        ActiveAnimator();
        CheckPlayerIsInmune();
        CheckIfPlayerIsAlive();
    }


    private void CheckIfPlayerIsAlive()
    {

        switch (GameManager.GetIsPlayerAlive())
        {
            case true:
                if (!GameManager.GetTimerIsRunning())
                {
                    if (gameObject.scene.name == "Mode1")
                    {
                        GameManager.SetLastSceneIndex(SceneManager.GetActiveScene().buildIndex);
                        SceneManager.LoadScene("Win");
                    }
                    else
                    {
                        GameManager.SetLastSceneIndex(SceneManager.GetActiveScene().buildIndex);
                        PlayerState = PlayerState.Lose;
                        SceneManager.LoadScene("Lost");
                    }
                }
                break;


            case false:

                if (gameObject.scene.name == "Mode2")
                {
                    GameManager.SetLastSceneIndex(SceneManager.GetActiveScene().buildIndex);
                    SceneManager.LoadScene("Win");

                }
                else
                {
                    GameManager.SetLastSceneIndex(SceneManager.GetActiveScene().buildIndex);
                    PlayerState = PlayerState.Lose;
                    SceneManager.LoadScene("Lost");
                }
                break;


        }

    }

    private void CheckPlayerIsInmune()
    {

        if (GameManager.GetPlayerIsInmune())
        {
            if (_timeCurrentInmune < 0)
            {
                GameManager.SetPlayerIsInmune(false);
                _timeCurrentInmune = _timeInmune;
            }
            else
            {
                _timeCurrentInmune -= Time.deltaTime;
                GameManager.SetPlayerIsInmune(true);
            }

        }

    }

    private void ActiveAnimator()
    {
        if (PlayerState == PlayerState.Idle)
        {
            animator.SetBool("Idle", true);
        }
        else
        {
            animator.SetBool("Idle", false);
        }
    }


}
