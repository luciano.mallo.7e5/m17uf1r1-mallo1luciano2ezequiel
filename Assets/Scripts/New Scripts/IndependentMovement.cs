using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndependentMovement : MonoBehaviour
{

    private float _speed = 2f;
    private float _maxDistance = 6f;
    private float _minDistance = -6F;
    private float _moveDirection = -1f;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // MovePlayer();
        MovePlayer2();
    }

    public float GetMoveDirection()
    {
        return _moveDirection;
    }
    /// <summary>
    /// Set de actual direction of the npc
    /// </summary>
    /// <param name="_moveDirection"> It's need to be a float 1 to go to right and -1 to go left</param>
    public void SetMoveDirection(float moveDirection)
    {
        _moveDirection = moveDirection;
        
    }
    private void MovePlayer2()
    {
        GameObject.Find("Player").GetComponent<PlayerData>().PlayerState = PlayerState.Walk;
        transform.position += new Vector3(_moveDirection * Time.deltaTime * _speed, 0, 0);
    }
    
    public void FlipCharecter()
    {

        if (_moveDirection == -1f) { 

            Quaternion target = Quaternion.Euler(0, 0f, 0f);
            transform.rotation = target;

        }

        if (_moveDirection == 1f)
        {
            Quaternion target = Quaternion.Euler(0, 180f, 0f);
            transform.rotation = target;
        }

    }


}
