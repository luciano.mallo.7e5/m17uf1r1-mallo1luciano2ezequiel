using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnCollitionWithPlayerKillPlayer : MonoBehaviour
{
    public Animator animator;

    [SerializeField] private float _explotionForce = 1000000000;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {


        if (collision.gameObject.tag == "Player")
        {

            if (!GameManager.GetPlayerIsInmune())
            {
                GameManager.SetPlayerAlive(false);
            }
        }
        if (collision.gameObject.tag == "Land")
        {

            Collider2D collider2D = GameObject.Find(this.name).GetComponent<Collider2D>();
            collider2D.isTrigger = true;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;

            animator.SetBool("EarthTouched", true);
            ExplosionDamage();// it's not working well, the force it's more powerfull more away from the object 
            Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        }
    }

    void ExplosionDamage()
    {
        Vector2 center = new Vector2(transform.position.x, transform.position.y);
        float radius = 2f;
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius);
        foreach (var hitCollider in hitColliders)
        {

            if (hitCollider.tag == "Player")
            {

                ExplosionAddForce(hitCollider.GetComponent<Rigidbody2D>());
            }
        }
    }
    public void ExplosionAddForce(Rigidbody2D rb)
    {
        var explosionDir = rb.position - new Vector2(transform.position.x, transform.position.y);
        var explosionDistance = explosionDir.magnitude;
        // rb.AddForce(explosionDir * _explotionForce);
        // rb.AddForce(new Vector2(explosionDistance *  _explotionForce, explosionDistance * _explotionForce ));

    }


}
