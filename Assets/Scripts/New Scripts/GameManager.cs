using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;
    private static bool _isPlayerAlive = true;
    private static string _playerName;
    private static bool _hardcoreMode;
    // private static float _playerSpeed = 5f;
    private static float _playerWeight = 2.5f;
    private static float _playerHeight = 5f;
    private static bool _grounded = false;
    private static RayFromPlayerToground _isGroundTouched;
    private static bool timerIsRunning;
    private static GameStates _gameStates;
    private static int _lastScene;
    private static bool _isInmune;


    public static bool GetTimerIsRunning()
    {
        return timerIsRunning;
    }
    public static void SetTimerState(bool isActive)
    {
        timerIsRunning = isActive;
    }
    public static void SetPlayerAlive(bool state)
    {
        _isPlayerAlive = state;

    }
    public static bool GetIsPlayerAlive()
    {
        return _isPlayerAlive;
    }
    public static void SetPlayerName(string playerName)
    {
        _playerName = playerName;

    }
    public static string GetPlayerName()
    {
        return _playerName;

    }
    public static float GetPlayerSpeed()
    {
        return CalculateSpeed(_playerWeight, _playerHeight);
    }

    public static bool GetIsGrounded()
    {
        return _grounded;
    }
    public static void SetIsGrounded(bool grounded)
    {
        _grounded = grounded;
    }

    public static float GetPlayerWeight()
    {
        return _playerWeight;
    }

    public static void SetPlayerWeight(float weight)
    {
        _playerWeight = weight;
    }

    public static float GetPlayerHeight()
    {
        return _playerHeight;
    }

    public static void SetPlayerHeight(float playerHeight)
    {
        _playerHeight = playerHeight;
    }

    public static float CalculateSpeed(float weight, float heigh)
    {
        return heigh / weight < 0 ? 350f : heigh / weight * 150;
    }

    public static void SetPlayerIsInmune(bool isInmune) 
    {
        _isInmune = isInmune;
    }
    public static bool GetPlayerIsInmune()
    {
        return _isInmune;
    }

    public static GameManager Instance
    {
        get
        {

            if (_instance is null)
            {
                Debug.LogError("GameManager MAnager is null");
            }
            return _instance;
        }
    }

    public static bool GetStateHardcordMode()
    {
        return _hardcoreMode;
    }
    public static bool SetStateHardcordMode(bool state)
    {
        return _hardcoreMode = state;
    }

    public static void SetGameState(GameStates gameStates)
    {
        _gameStates = gameStates;

    }
    public static GameStates GetGameState()
    {
        return _gameStates;

    }
    public static int GetLastSceneIndex()
    {
       return _lastScene;
    }
    public static void SetLastSceneIndex(int scene)
    {
        _lastScene = scene;
    }

    public static void ResetGameManager()
    {
    _isPlayerAlive = true;
    _hardcoreMode = false;
    _playerWeight = 2.5f;
    _playerHeight = 5f;
    _grounded = false;
    timerIsRunning=false;
    _gameStates = GameStates.Normal;
}
    private void Awake()
    {
        _instance = this;
        _isPlayerAlive = true;
        _gameStates = GameStates.Normal;

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
