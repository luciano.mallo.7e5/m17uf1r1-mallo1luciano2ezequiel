using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipFireBalls : IndependentMovement
{
    private RaycastHit2D ground;
    private Vector3 _playerPosition;
    private IndependentMovement independentMovementOb;
    // Start is called before the first frame update
    void Start()
    {
        _playerPosition = GameObject.Find("Player").GetComponent<Transform>().position;
        independentMovementOb = GameObject.Find("Player").GetComponent<IndependentMovement>();
    }

    private void FixedUpdate()
    {
        ground = Physics2D.Raycast(transform.position, Vector2.down, (gameObject.GetComponent<BoxCollider2D>().size.y / 2) + 0.4f);
        ControlFallingOutOfLand();
    }

    // Update is called once per frame
    void Update()
    {

        GoBeforePlayer();
    }


    void GoBeforePlayer()
    {
        _playerPosition = GameObject.Find("Player").GetComponent<Transform>().position;

        if (independentMovementOb.GetMoveDirection() == -1)
        {
            transform.position = new Vector3(_playerPosition.x - 1f, _playerPosition.y, _playerPosition.z);
        }
        if (independentMovementOb.GetMoveDirection() == 1)
        {
            transform.position = new Vector3(_playerPosition.x + 1f, _playerPosition.y, _playerPosition.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Fireball"))
        {
            independentMovementOb.SetMoveDirection(independentMovementOb.GetMoveDirection() == 1 ? -1 : 1);
            independentMovementOb.FlipCharecter();
        }
    }
    private void ControlFallingOutOfLand()
    {
        try
        {
            if (ground.collider.tag == "Land")
            {
                GameManager.SetIsGrounded(true);
            }
        }
        catch
        {
            GameManager.SetIsGrounded(false);
            independentMovementOb.SetMoveDirection(independentMovementOb.GetMoveDirection() == 1 ? -1 : 1);
            independentMovementOb.FlipCharecter();
        }

    }

}
