using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesapearMushroom : MonoBehaviour
{
    public Animator animator;
    private float timeMushroomToDisappear = 5f;
    private float currentTimeToDisappear;
    private Rigidbody2D rigidBody;
    // Start is called before the first frame update
    void Start()
    {
        currentTimeToDisappear = timeMushroomToDisappear;
        rigidBody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Stop the mushroon when land and activate the animation 
        if (collision.GetComponent<Collider2D>().tag == "Land")
        {
            rigidBody.constraints = RigidbodyConstraints2D.FreezePosition;

            currentTimeToDisappear -= Time.deltaTime;

            if (currentTimeToDisappear > 0)
            {
                currentTimeToDisappear -= Time.deltaTime;
            }
            else
            {
                animator.SetBool("Disappear", true);
                Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            }
        }
        // Activate the animation when player take it and change the player state or game state.
        if (collision.gameObject.tag == "Player")
        {
            if (gameObject.scene.name == "Mode1")
            {
                GameManager.SetGameState(GameStates.Growing);
            }
            else 
            {
                GameManager.SetPlayerIsInmune(true);
            }


            animator.SetBool("Disappear", true);
            Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        }



    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().tag == "Land")
        {

            currentTimeToDisappear -= Time.deltaTime;

            if (currentTimeToDisappear > 0)
            {
                currentTimeToDisappear -= Time.deltaTime;
            }
            if (currentTimeToDisappear < 2)
            {
                animator.SetBool("Disappear", true);
            }
            else
            {

                Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            }
        }


    }


}
