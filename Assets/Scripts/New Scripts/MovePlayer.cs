using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] public bool isgrounded;
    private Transform _PlayerTransform;
    private PlayerData _playerData;
    private Rigidbody2D _playerRigidBody;
    private float _jumpForce = 15f;

    // Start is called before the first frame update
    void Start()
    {
        _PlayerTransform = GameObject.Find("Guard").transform;
        _playerData = GameObject.Find("Guard").GetComponent<PlayerData>();
        _playerRigidBody = GameObject.Find("Guard").GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        PlayerJump();
    }

    // Update is called once per frame
    void Update()
    {
        isgrounded = GameManager.GetIsGrounded();
        _speed = GameManager.GetPlayerSpeed();
        ChangeStateOfPlayer();
        FlipCharecter();
        PlayerJump();
    }

    private void ChangeStateOfPlayer()
    {
        switch (Input.GetAxis("Horizontal"))
        {

            case 1:
            case -1:
                _playerData.PlayerState = PlayerState.Walk;
                MoveCharecter();
                break;
            case 0:
                _playerData.PlayerState = PlayerState.Idle;
                break;

        }
    }
    private void MoveCharecter()
    {
        // transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * _speed, 0, 0);
        _playerRigidBody.AddForce(new Vector2(Input.GetAxis("Horizontal") * _speed * Time.deltaTime, 0));
    }


    private void FlipCharecter()
    {
        if (Input.GetAxis("Horizontal") == -1)
        {

            Quaternion target = Quaternion.Euler(0, 0f, 0f);
            _PlayerTransform.rotation = target;

        }

        if (Input.GetAxis("Horizontal") == 1)
        {
            Quaternion target = Quaternion.Euler(0, 180f, 0f);
            _PlayerTransform.rotation = target;
        }

    }


    private void PlayerJump()
    {
        
        if (GameManager.GetIsGrounded())
        {
            
            if (Input.GetKey(KeyCode.Space))
            {
                _playerRigidBody.AddForce(new Vector2(0, _jumpForce * Time.deltaTime),ForceMode2D.Impulse);
                {
                    if (Mathf.Abs(Input.GetAxis("Horizontal")) < _speed * Time.deltaTime)
                    {
                        _playerRigidBody.AddForce(new Vector2(Input.GetAxis("Horizontal") * _speed * Time.deltaTime, 0));
                    }
                }
            }
        }



    }
}
