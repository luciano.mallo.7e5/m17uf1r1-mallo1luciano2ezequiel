using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{

    private float timer = 60f;
    private string timeText;
    private bool isTimerRunning = false;

    // Start is called before the first frame update
    void Start()
    {
        ControlTimer();
        timeText = GetComponent<TMP_Text>().text;
       
    }

    // Update is called once per frame
    public void Update()
    {
        ControlTimer();

    }

    private void ControlTimer()
    {
        if (timer > 0)
        {

            GameManager.SetTimerState(true);
            timer -= Time.deltaTime;
            timeText = Mathf.Round(timer).ToString();
            GetComponent<TMP_Text>().text = timeText;
            if (timer < 30)
            {
                GameManager.SetStateHardcordMode(true);
            }
        }
        else
        {
            GameManager.SetTimerState(false);

        }
    }
}
