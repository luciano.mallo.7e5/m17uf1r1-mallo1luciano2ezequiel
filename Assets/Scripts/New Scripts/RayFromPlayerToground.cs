using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayFromPlayerToground : MonoBehaviour
{
    RaycastHit2D[] colliders;
    // Start is called before the first frame update
    void Start() { }
    private void FixedUpdate()
    {
        // ground = Physics2D.Raycast(transform.position, Vector2.down, (gameObject.GetComponent<BoxCollider2D>().size.y / 2) + 0.4f);
        colliders = Physics2D.RaycastAll(transform.position, Vector2.down, (gameObject.GetComponent<BoxCollider2D>().size.y / 2) + 0.4f);
        IsGroundTouch();
    }
    void Update()
    {

        // Debug.DrawRay(transform.position, Vector2.down, color: Color.green);

    }


    public void IsGroundTouch()
    {

        foreach (RaycastHit2D ground in colliders)
        {
            if (ground.collider.tag == "Land")
            {
                GameManager.SetIsGrounded(true);
            }
            else

            {
                GameManager.SetIsGrounded(false);
            }

        }
    }

    // Update is called once per frame

}
