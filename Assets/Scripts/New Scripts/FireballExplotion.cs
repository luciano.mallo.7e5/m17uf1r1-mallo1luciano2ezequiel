using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballExplotion : MonoBehaviour
{

    public Animator animator;
    public GameObject fireballPrefab;
    public float fireballLife = 10f;
    // public GameObject fireBall;

    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        animator.SetBool("EarthTouched", true);
    }
  
}
