using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFireballFromClick : MonoBehaviour
{
    public float timeToSpawn = 1f;
    public GameObject[] objectsToInstantiate;
    private Camera cam;
    private float whereToCreateFireballOnY = 4.6f;
    private int clicksCounter;
    private float currentTimeToSpawn;

    private void Awake()
    {
        cam = Camera.main;
    }
    private void Update()
    {
        currentTimeToSpawn -= Time.deltaTime;

        if (Input.GetButtonDown("Fire1"))
        {


            if (CanShoot())
            {
                clicksCounter++;
                if (clicksCounter > 10)
                {
                    clicksCounter = 0;
                    SpawnObject(1, cam.ScreenToWorldPoint(Input.mousePosition));
                }
                else
                {
                    SpawnObject(0, cam.ScreenToWorldPoint(Input.mousePosition));
                }
            }
        }

    }


    private bool CanShoot()
    {
        if (currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;
            return false;
        }
        else
        {
            currentTimeToSpawn = timeToSpawn;
            return true;
        }

    }

    private void SpawnObject(int numOrderObjectToSpawn, Vector3 mousePosition)
    {
        mousePosition.z = 0;
        mousePosition.y = whereToCreateFireballOnY;
        Instantiate(objectsToInstantiate[numOrderObjectToSpawn], mousePosition, Quaternion.identity);
    }

}
