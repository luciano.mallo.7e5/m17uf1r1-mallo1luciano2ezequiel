using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneOfMode : MonoBehaviour
{

    private Button _buttonMode1;
    private Button _buttonMode2;
    private string _playerName;




    // Start is called before the first frame update
    void Start()
    {
       
        _buttonMode1 = GameObject.Find("Mode1Button").GetComponent<Button>();
        _buttonMode2 = GameObject.Find("Mode2Button").GetComponent<Button>();
         
        _buttonMode1.onClick.AddListener(LoadSceneMode1);
        _buttonMode2.onClick.AddListener(LoadSceneMode2);
    }

    public void LoadSceneMode1(){
        _playerName = GameObject.Find("Name").GetComponent<Text>().text;
        GameManager.SetPlayerName(_playerName);
        SceneManager.LoadScene("Mode1");
    }
    public void LoadSceneMode2()
    {
        _playerName = GameObject.Find("Name").GetComponent<Text>().text;
        GameManager.SetPlayerName(_playerName);
        SceneManager.LoadScene("Mode2");
    }





    // Update is called once per frame
    void Update()
    {
        
    }
}
