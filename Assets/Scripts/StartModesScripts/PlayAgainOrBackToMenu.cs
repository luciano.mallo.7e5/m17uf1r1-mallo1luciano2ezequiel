using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayAgainOrBackToMenu : MonoBehaviour
{
    private Button _buttonBackToMenu;
    private Button _buttonPlayAgain;
    

    // Start is called before the first frame update
    void Start()
    {
        
        _buttonBackToMenu = GameObject.Find("BackToMenuBtn").GetComponent<Button>();
        _buttonPlayAgain = GameObject.Find("PlayAgainBtn").GetComponent<Button>();
        GameObject.Find("PlayerName").GetComponent<Text>().text = GameManager.GetPlayerName();

        _buttonBackToMenu.onClick.AddListener(LoadSceneBackToMenu);
        _buttonPlayAgain.onClick.AddListener(LoadScenePlayAgain);
    }

    public void LoadSceneBackToMenu()
    {
        GameManager.ResetGameManager();
        SceneManager.LoadScene("StarGame");
    }
    public void LoadScenePlayAgain()
    {
        
        var playagainScene = GameManager.GetLastSceneIndex();
        GameManager.ResetGameManager();
        SceneManager.LoadScene(playagainScene);
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
