using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeItGiant : MonoBehaviour
{
    private GameObject _player;
    private PlayerData _playerData;
    private Vector3 _scaleChange = new Vector3(+0.01f, +0.01f, 0.0f);
    private Vector3 _maxScale;
    private float _proportionScale = 2f;
    private float _maxHeight;
    private float _coolDownTime = 0;
    private float _coolDownValue = 3f;
    private float _orignalWeight;
    private float _orignalHeight;

    private void Awake()
    {
        _playerData = GameObject.Find("Guard").GetComponent<PlayerData>();
    }
    // Start is called before the first frame update
    void Start()
    {
        _maxHeight = GameManager.GetPlayerHeight();
        _orignalHeight = GameManager.GetPlayerHeight();
        _maxScale = new Vector3(_maxHeight, _maxHeight, _proportionScale);

    }

    // Update is called once per frame
    void Update()
    {
        ChangeGameState();
        MakeGiantOrNormal();
    }


    private void MakeGiantOrNormal()
    {

        switch (GameManager.GetGameState())
        {

            case GameStates.Normal:
                break;

            case GameStates.GoingDown:
              if (Mathf.Round(Vector3.Distance(transform.localScale, new Vector3(_maxHeight, _maxHeight, 0f))) <= _maxHeight)
                {
                    transform.localScale -= new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 0.0f);
                    GameManager.SetPlayerWeight(GameManager.GetPlayerWeight() - 5f * Time.deltaTime);
                    GameManager.SetPlayerHeight(GameManager.GetPlayerHeight() - 5f * Time.deltaTime);
                }
                else 
                {
                    transform.localScale = new Vector3(1f, 1f, 0);
                    GameManager.SetGameState(GameStates.Normal);
                    GameManager.SetPlayerWeight(_orignalWeight);
                    GameManager.SetPlayerHeight(_orignalHeight);
                }
                
                break;

          

            case GameStates.Growing:
                if (Mathf.Round(Vector3.Distance(transform.localScale, new Vector3(_maxHeight, _maxHeight, 0f))) > 1f)
                {
                    transform.localScale += new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 0.0f);
                    GameManager.SetPlayerWeight(GameManager.GetPlayerWeight() + 5f * Time.deltaTime);
                    GameManager.SetPlayerHeight(GameManager.GetPlayerHeight() + 5f * Time.deltaTime);
                }
                else
                {

                    GameManager.SetGameState(GameStates.Giant);
                }

                break;


            case GameStates.Giant:

                GameManager.SetGameState(GameStates.GoingDown);

                break;

        }

    }

    private void ChangeGameState()
    {
        if (GameManager.GetGameState() == GameStates.Normal) _coolDownTime += Time.deltaTime;

        if (_coolDownTime > _coolDownValue)
        {
            if (GameManager.GetGameState() == GameStates.Growing)
            {
                _orignalWeight = GameManager.GetPlayerWeight();
                _coolDownTime = 0f;  
            }

        }

    }

}
